#!/usr/bin/env php
<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Finder\Finder;

$autoloadFiles = [__DIR__ . '/../vendor/autoload.php', __DIR__ . '/../../../autoload.php'];

foreach ($autoloadFiles as $autoloadFile) {
    if (file_exists($autoloadFile)) {
        require_once $autoloadFile;
    }
}

$console = new Application('WeBird Mongo migration tool', '0.01');
$console->add(new \WeBird\MongoMigrations\Command\ApplyMigrations());
$console->add(new \WeBird\MongoMigrations\Command\RollbackMigrations());

$console->run();