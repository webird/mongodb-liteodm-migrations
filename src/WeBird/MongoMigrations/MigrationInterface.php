<?php

namespace WeBird\MongoMigrations;


use MongoDB\Database;

interface MigrationInterface
{

    public function getDatabaseAlias();

    public function apply(Database $database);

    public function rollback(Database $database);

}