<?php

namespace WeBird\MongoMigrations\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WeBird\LiteOdm\ServiceContainer;
use WeBird\MongoMigrations\MigrationInterface;

class RollbackMigrations extends AbstractMigrationCommand
{
    public function configure()
    {
        parent::configure();
        $this->setName('rollback')
            ->addArgument('number', InputOption::VALUE_REQUIRED, 'Number of migrations for rollback', 1);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initContainer($input);
        $allMigrations = $this->readMigrations($input);

        foreach ($allMigrations as $alias => $migrations) {
            $database = ServiceContainer::instance()->getDatabase($alias);

            $migrationRegisterCollection = $database->selectCollection(static::MIGRATION_REGISTER_COLLECTION);

            $applied = $migrationRegisterCollection
                ->find(['_id' => ['$in' => array_keys($migrations)]])
                ->toArray();

            $appliedMigrations = [];

            foreach ($applied as $migration) {
                $appliedMigrations[$migration['_id']] = $migrations[$migration['_id']];
            }

            krsort($appliedMigrations);

            $appliedMigrations = array_slice($appliedMigrations, 0, $input->getArgument('number'));

            /**
             * @var string $id
             * @var MigrationInterface $migration
             */
            foreach ($appliedMigrations as $id => $migration) {
                $class = get_class($migration);
                $output->write("<info>Rollback migration \"{$class}\"...</info>");

                $migration->rollback($database);

                $output->writeln("<info>DONE</info>");
                $migrationRegisterCollection->deleteOne(['_id' => $id]);
            }
        }
    }

}