<?php

namespace WeBird\MongoMigrations\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WeBird\LiteOdm\ServiceContainer;
use WeBird\MongoMigrations\MigrationInterface;

class ApplyMigrations extends AbstractMigrationCommand
{

    public function configure()
    {
        parent::configure();
        $this->setName('apply');

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initContainer($input);
        $allMigrations = $this->readMigrations($input);

        foreach ($allMigrations as $alias => $migrations) {

            $database = ServiceContainer::instance()->getDatabase($alias);

            $migrationRegisterCollection = $database->selectCollection(static::MIGRATION_REGISTER_COLLECTION);

            $applied = $migrationRegisterCollection
                ->find(['_id' => ['$in' => array_keys($migrations)]])
                ->toArray();

            foreach ($applied as $migration) {
                unset($migrations[$migration['_id']]);
            }

            ksort($migrations);

            /** @var MigrationInterface $migration */
            foreach ($migrations as $id => $migration) {
                $class = get_class($migration);
                $output->write("<info>Apply migration \"{$class}\"...</info>");

                $migration->apply($database);

                $output->writeln("<info>DONE</info>");

                $migrationRegisterCollection->insertOne([
                    '_id' => $id,
                    'class' => $class,
                    'time' => new \MongoDB\BSON\UTCDatetime(microtime(true))
                ]);
            }
        }
    }

}