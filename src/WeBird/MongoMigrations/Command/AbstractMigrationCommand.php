<?php

namespace WeBird\MongoMigrations\Command;

use Doctrine\Common\Annotations\AnnotationReader;
use MongoDB\Client;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use WeBird\LiteOdm\ServiceContainer;
use WeBird\MongoMigrations\MigrationInterface;

class AbstractMigrationCommand extends Command
{
    const MIGRATION_REGISTER_COLLECTION = '__webird_migrations';

    public function configure()
    {
        $this
            ->addOption('url', 'u', InputOption::VALUE_REQUIRED, 'Mongo URL')
            ->addOption('databaseAlias', 'A', InputOption::VALUE_REQUIRED, 'Mongo URL alias', 'default')
            ->addOption('database', 'B', InputOption::VALUE_REQUIRED, 'Mongo database')
            ->addOption('config', 'C', InputOption::VALUE_REQUIRED, 'Path to configuration file')
            ->addOption('directory', 'D', InputOption::VALUE_REQUIRED, 'Path to directory of migrations', './migrations');
    }

    protected function initContainer(InputInterface $input)
    {
        if ($input->getOption('config')) {
            $path = realpath($input->getOption('config'));
            try {
                if ($path) {
                    $configuration = Yaml::parse(file_get_contents($path));
                } else {
                    throw new \Exception('File not found');
                }

                if (empty($configuration['databases'])) {
                    throw new \Exception('File is incomplete. There are no "databases" section');
                }

                $databases = [];
                foreach ($configuration['databases'] as $dbAlias => $dbConfigurations) {
                    $client = isset($dbConfigurations['url']) ? new Client($dbConfigurations['url']) : new Client();
                    $databases[$dbAlias] = $client->selectDatabase($dbConfigurations['db']);
                }
            } catch (Exception $ex) {
                throw new \RuntimeException(
                    'Unable to read configuration file "' . $input->getOption('config') . '"',
                    null,
                    $ex
                );
            }
        } else {
            if (!$input->getOption('database')) {
                throw new \RuntimeException('"database" option is required, then "config" is now set');
            }

            $client = ($input->getOption('url') ? new Client($input->getOption('url')) : new Client());

            $databases = [
                $input->getOption('databaseAlias') => $client->selectDatabase($input->getOption('database'))
            ];
        }

        new ServiceContainer(
            new AnnotationReader(),
            $databases
        );
    }

    protected function readMigrations(InputInterface $input)
    {
        if ($input->getOption('config')) {
            $path = realpath($input->getOption('config'));
            if ($path) {
                $directory = Yaml::parse(file_get_contents($path))['directory'];
            } else {
                throw new \RuntimeException('Unable to open configuration file "' . $input->getOption('config') . '"');
            }
        } else {
            $directory = $input->getOption('directory');
        }

        $finder = Finder::create();
        $finder
            ->files()
            ->name("*.php")
            ->in($directory);

        $migrations = [];
        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            require_once $file->getRealPath();
            $class = $file->getBasename('.php');

            /** @var MigrationInterface $migration */
            $migration = new $class();

            if (
                $migration instanceof MigrationInterface &&
                ServiceContainer::instance()->hasDatabase($migration->getDatabaseAlias())
            ) {
                $migrations[$migration->getDatabaseAlias()][$migration->getDatabaseAlias() . ':' . $class] = $migration;
            }
        }

        return $migrations;
    }
}
